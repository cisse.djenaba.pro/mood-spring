package com.mood.login.security.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mood.login.dao.MoodRepository;
import com.mood.login.dao.UserRepository;
import com.mood.login.modele.Mood;
import com.mood.login.modele.User;

@Service
public class MoodServiceImpl {
	
	@Autowired
	MoodRepository moodRepository; 
	
	@Autowired
	UserRepository userRepository; 
	
	public void addMood(String mood, Short moodDescription, String location, String category, Long userId, String physicalMood) {
		
		User user = userRepository.findById(userId).get(); 
		Mood moodToAdd = new Mood( mood, moodDescription,  location, category, physicalMood ); 
		
		user.getMoods().add(moodToAdd); 
		
		userRepository.save(user);
		
	}
	
	

}
