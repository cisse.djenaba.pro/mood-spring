package com.mood.login.modele;

public enum CategoryMoodModel {
	
	MOOD_MUSIC, 
	MOOD_SPORT, 
	MOOD_MEDITATION, 
	MOOD_CULTURE,
	MOOD_ART

}
