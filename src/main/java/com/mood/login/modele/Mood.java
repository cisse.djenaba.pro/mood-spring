package com.mood.login.modele;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.NaturalId;

@Entity
@Table(name = "moods")
public class Mood {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;


	@Column(length = 60)
	private String moodModel;

	private Short budget;

	private String location;

	private String category;

	private String physicalMood;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable = false)
	private Date lastUpdate;

	@PrePersist
	private void onCreate() {
		lastUpdate = new Date();
	}

	public Mood() {

	}

	public Mood(String moodModel, Short budget, String location, String category, String physicalMood) {
		this.moodModel = moodModel;
		this.budget = budget;
		this.location = location;
		this.category = category;
		this.physicalMood = physicalMood; 

	}


	public String getMoodModel() {
		return moodModel;
	}

	public void setMoodModel(String moodModel) {
		this.moodModel = moodModel;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public Short getBudget() {
		return budget;
	}

	public void setBudget(Short budget) {
		this.budget = budget;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}


	public String getPhysicalMood() {
		return physicalMood;
	}

	public void setPhysicalMood(String physicalMood) {
		this.physicalMood = physicalMood;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

}
