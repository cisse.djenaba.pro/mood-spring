package com.mood.login.modele;

public enum MoodModel {
	
	MOOD_VERYBAD,
	MOOD_BAD, 
	MOOD_NEUTRAL, 
	MOOD_GOOD,
	MOOD_VERYGOOD

}
