package com.mood.login.http.controller;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mood.login.dao.MoodRepository;
import com.mood.login.dao.UserRepository;
import com.mood.login.message.request.MoodForm;
import com.mood.login.message.response.ResponseMessage;
import com.mood.login.modele.Mood;
import com.mood.login.modele.MoodModel;
import com.mood.login.modele.User;
import com.mood.login.security.services.MoodServiceImpl;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/auth")
public class MoodRestAPIs {

	
	@Autowired
	MoodRepository moodRepository; 
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	MoodServiceImpl moodService;

	
    @GetMapping
    public String main(Model model) {
        model.addAttribute("mood", new Mood());
        return "index";
    }
	
     
     @GetMapping("/moods")
     public List<Mood> displayMessage() {
    	 return moodRepository.findAll(); 
     }
     
     
     @GetMapping("/usermood/{userId}")
     @ResponseBody
     public Set<Mood> userMood(@PathVariable (name="userId", required = false) Long userId) {
    	 
    	 User user = userRepository.findById(userId).get(); 
    	 
    	 return user.getMoods() ; 
    
     }
     
     @PostMapping("/addmood2/{mood}/{budget}/{username}/{location}/{category}/{physicalMood}")
     @ResponseBody
     public void addMood2 (@PathVariable (name = "mood", required = false)String mood, @PathVariable (name = "budget", required = false) Short budget, @PathVariable (name = "username", required = false) String username, @PathVariable (name = "location", required = false) String location, @PathVariable (name = "category", required = false) String category, @PathVariable (name = "physicalMood", required = false) String physicalMood) {
    	 

    	 
    	 System.out.println("INPUT MOOD : " + mood);
    	 System.out.println("INPUT USER : " + username);
    	 System.out.println("INPUT LOCATION : " + location);
    	 System.out.println("INPUT BUDGET : " + budget);
    	 System.out.println("INPUT CATEGORY : " + category);
    	 System.out.println("INPUT PHYSICALMOOD : " + physicalMood);

    	 
  
    	 User user = userRepository.findByUsername(username).get(); 
    	 
    	 Mood addedMood = new Mood(mood, budget, location, category, physicalMood);
    	 user.getMoods().add(addedMood);
    	 
    	 userRepository.save(user);
    	 
    	 
     }
 
	
}
