package com.mood.login.dao;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;

import com.mood.login.message.request.MoodForm;
import com.mood.login.modele.Mood;



@Repository
public interface MoodRepository extends JpaRepository <Mood, Long>{
	
	//Optional<Mood> findByName(MoodName moodName);

	static final Map<Long, Mood> moodMap = new HashMap<Long, Mood>();
	
    public default Mood addMood(MoodForm moodRequest) {
        Mood newMood = new Mood();  
        
        moodMap.put(newMood.getId(), newMood);
        return newMood;
    }
 

}
