package com.mood.login.dao;


import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

import com.mood.login.modele.Mood;
import com.mood.login.modele.User;


/*public interface UserRepository extends CrudRepository<User, Integer> {

}*/

@Repository
public interface UserRepository extends JpaRepository<User, Long>{
	
	
	    Optional<User> findByUsername(String username);
	    Optional<User> findById(Long userId);
	    Boolean existsByUsername(String username);
	    Boolean existsByEmail(String email);
		void save(Mood addedMood);


}
